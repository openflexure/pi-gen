#!/bin/bash -e

# Install a pre-built Python environment with the microscope client in it

ofm_client_scripts_dir="/home/${FIRST_USER_NAME}/microscope_python_scripts"
ofm_client_venv="${ofm_client_scripts_dir}/.venv"

# We'll put this in a folder in the default user's home directory
mkdir -p "${ofm_client_scripts_dir}"
cd "${ofm_client_scripts_dir}"

# Make a virtual environment and install the python client
python3 -m venv "$ofm_client_venv" --prompt "OFM Client"
source "$ofm_client_venv"/bin/activate
pip install --only-binary=:all: openflexure-microscope-client
pip install jupyter notebook matplotlib
deactivate

chown -R ${FIRST_USER_NAME} "${ofm_client_scripts_dir}"
