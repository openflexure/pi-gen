#!/bin/bash -e

mkdir -p files
wget https://gitlab.com/openflexure/openflexure-microscope-cli/-/raw/master/ofm -O files/ofm
wget https://gitlab.com/openflexure/openflexure-microscope-cli/-/raw/master/ofm-init -O files/ofm-init

install -v -m 755 files/ofm-init "${ROOTFS_DIR}/usr/bin/ofm-init"
install -v -m 755 files/ofm "${ROOTFS_DIR}/usr/bin/ofm"

on_chroot << EOF
echo "source /usr/bin/ofm-init" >> "/home/${FIRST_USER_NAME}/.bashrc"
echo "source /usr/bin/ofm-init" >> "/home/${OPENFLEXURE_USER_NAME}/.bashrc"
echo "source /usr/bin/ofm-init" >> "/root/.bashrc"
EOF