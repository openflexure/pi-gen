# pi-gen

_Tool used to create the build.openflexure.org Raspbian images_

This repository was forked from upstream [pi-gen] and modified with various OpenFlexure-specific configuration and install routines, to create the OpenFlexure Pi OS.  We have neatened this up and moved to a new repository at [openflexure-pi-os], which makes it much clearer what code is ours and what came from upstream. All new development will take place on the new repository, this repository will be archived and will not receive bug fixes, as the OS images built by the new repository should be equivalent - it is the organisation of the build script rather than the content of the end result that has changed.

[pi-gen]: https://github.com/RPi-Distro/pi-gen/
[openflexure-pi-os]: https://gitlab.com/openflexure/openflexure-pi-os/