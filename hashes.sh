for file in $(find "./deploy" -name "*-raspbian-openflexure-*.zip" -exec basename {} \;); do
    echo $file
    echo -e "${CI_COMMIT_REF_SLUG}\nPIPELINE ${CI_PIPELINE_ID}\nJOB ${CI_JOB_ID}\nSTARTED BY ${GITLAB_USER_LOGIN}" > "./deploy/$file.ci"
    (cd deploy; sha256sum "$file" > "$file.sha256")
done