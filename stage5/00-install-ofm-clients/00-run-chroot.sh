#!/bin/bash -e

# Extras
jsclient_url="https://build.openflexure.org/openflexure-ev/latest/linux/armv7l"

# OpenFlexure Connect

# Create Applications directory if it doesn't exist
mkdir -p "/home/${FIRST_USER_NAME}/Applications"
# Define the path where our AppImage will live
appimagepath="/home/${FIRST_USER_NAME}/Applications/openflexure-ev.AppImage"
# Download AppImage to $appimagepath
wget "$jsclient_url" -O "$appimagepath"
# Make executable
chmod a+x "$appimagepath"
# Fix ownership of appimage files
chown -R "${FIRST_USER_NAME}:${FIRST_USER_NAME}" "/home/${FIRST_USER_NAME}/Applications"

# Download shortcut icon
mkdir -p "/home/${FIRST_USER_NAME}/.icons"
wget "https://gitlab.com/openflexure/openflexure-connect/-/raw/master/build/icons/48x48.png" -O "/home/${FIRST_USER_NAME}/.icons/openflexure-ev.png"
chown -R "${FIRST_USER_NAME}:${FIRST_USER_NAME}" "/home/${FIRST_USER_NAME}/.icons"

# Create app menu shortcut
echo | tee -a "/home/${FIRST_USER_NAME}/.local/share/applications/appimagekit-openflexure-ev.desktop" <<- EOL
[Desktop Entry]
Name=OpenFlexure Connect
Comment=An electron-based user client for the OpenFlexure Microscope Server
Exec="$appimagepath" %U
Terminal=false
Type=Application
StartupWMClass=OpenFlexure Connect
Categories=Science;
Icon=openflexure-ev
TryExec=$appimagepath
EOL

# Copy shortcut to desktop
mkdir -p "/home/${FIRST_USER_NAME}/Desktop"
cp "/home/${FIRST_USER_NAME}/.local/share/applications/appimagekit-openflexure-ev.desktop" "/home/${FIRST_USER_NAME}/Desktop/appimagekit-openflexure-ev.desktop"
